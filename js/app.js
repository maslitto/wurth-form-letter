"use strict";

//= vendor/jquery.min.js
//= ../node_modules/magnific-popup/dist/jquery.magnific-popup.js
//= ../node_modules/mobile-detect/mobile-detect.js
//= ../node_modules/jquery.add-input-area/dist/jquery.add-input-area.min.js
var app = (function () {
	return {
		init: function () {
            app.createNumberType();
            app.initPopup();
            app.popupClose();
            app.initInputArea();
            app.form();
		},
        createNumberType: function () {
            $('.js-form__number').each(function () {
                var $input = $(this),
                    $wrapper = $(this).closest('.js-form__number-wrapper'),
                    $plus = $wrapper.find('.js-form__number-plus'),
                    $minus = $wrapper.find('.js-form__number-minus'),
                    min = parseFloat($input.attr('min')),
                    max = parseFloat($input.attr('max')),
                    step = $input.attr('step') ? parseFloat($input.attr('step')) : 1,
                    oldValue = parseFloat($input.val());
                $input.data('start-value', $input.val());
                $input.data('prev-value', $input.val());
                if (oldValue >= max) {
                    $input.val(max);
                    $plus.addClass('maxed');
                } else if (oldValue <= min) {
                    $input.val(min);
                    $minus.addClass('maxed');
                }
                $input.change(function () {
                    oldValue = parseFloat($(this).val());
                    if (oldValue >= max) {
                        $input.val(max);
                        $plus.addClass('maxed');
                        $minus.removeClass('maxed');
                        oldValue = max;

                    } else if (oldValue <= min) {
                        $input.val(min);
                        $minus.addClass('maxed');
                        $plus.removeClass('maxed');
                        oldValue = min;
                    }
                    oldValue = ( $(this).val() - $(this).data('prev-value') ) ;
                    $(this).data('prev-value', $(this).val());
                });
                $plus.click(function () {
                    oldValue = parseFloat($input.val());
                    $minus.removeClass('maxed');
                    if (oldValue >= max) {
                        $input.val(oldValue);
                        $plus.addClass('maxed');
                    } else {
                        $plus.removeClass('maxed');
                        $input.val(oldValue + step);
                        $input.data('prev-value', +$input.data('prev-value') + step );
                        if (oldValue + step >= max) {
                            $plus.addClass('maxed');
                        }
                        $input.trigger('change');
                    }
                });
                $minus.click(function () {
                    oldValue = parseFloat($input.val());
                    $plus.removeClass('maxed');
                    if (oldValue <= min) {
                        $input.val(oldValue);
                        $minus.addClass('maxed');
                    } else {
                        $minus.removeClass('maxed');
                        $input.val(oldValue - step);
                        $input.data('prev-value', +$input.data('prev-value') - step );
                        if (oldValue - step <= min) {
                            $minus.addClass('maxed');
                        }
                        $input.trigger('change');
                    }
                });
            });
        },

        initPopup: function () {
            $(document).on("click", ".js-popup", function(event) {
                $.magnificPopup.open({
                    items: {
                        src: '#modal', // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }
                })
            });

        },
        popupClose: function () {
            $('.js-mfp-close').click(function () {
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            });
        },
        initInputArea: function() {
            $('.js-add-input-area').addInputArea({
                area_var: '.js-add-input-area-row',
                btn_add: '.js-area-add',
                btn_del: '.js-area-remove',
                maximum : 3,
                after_add: function () {
                    $('.js-form__number').unbind('change');
                    $('.js-form__number-plus').unbind('click');
                    $('.js-form__number-minus').unbind('click');
                    app.createNumberType();
                }

            });
            console.log('start input area');
        },
        form: function () {
            $('.js-form').on('submit',function (e) {
                e.preventDefault();
                var errorFlag = false;
                var $form = $(this),
                    $inputs = $('.js-text-input'),
                    $numbers = $('.js-form__number');
                $inputs.each(function () {
                    var $wrapper = $(this).closest('.js-input-wrapper'),
                        $error = $wrapper.find('.js-error');
                    if($(this).val().length == 0){
                        errorFlag = true;
                        $error.addClass('open');
                    } else{
                        if($error.hasClass('open')){
                            $error.removeClass('open');
                        }

                    }
                });
                $numbers.each(function () {
                    var $wrapper = $(this).closest('.js-input-wrapper'),
                        $error = $wrapper.find('.js-error'),
                        min = parseInt($(this).attr('min')),
                        max = parseInt($(this).attr('max')),
                        value = parseInt($(this).val());

                    if(isNaN(parseFloat(value))){value = 0}
                    //console.log(value);

                    if(value < min || value > max){
                        errorFlag = true;
                        $error.text('число от '+min+' до '+max);
                        $error.addClass('open');
                    } else{
                        if($error.hasClass('open')){
                            $error.removeClass('open');
                        }

                    }
                });
                //console.log(errorFlag);
                if(!errorFlag){
                    $.ajax({
                        type:  'POST',
                        url: $(this).attr('action'),
                        data: $form.serializeArray(),
                        dataType: "json",
                        success: function(resp) {

                        },
                        error: function() {

                        }
                    });
                    $('.js-invite').hide();
                    $('.js-success').show();
                }
            })
        }
	}

})();

$(document).on('ready', app.init);






